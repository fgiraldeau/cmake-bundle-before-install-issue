# CMake fixup_bundle() is executed before installing the target

Minimal example to reproduce the fixup_bundle issue executed before the actual install of the target.

This behavior occurs when the target is defined in a subdirectory and the parent cmake contains the `install(CODE)`, even though the `install(CODE)` is defined after the `add_subdirectory()`.

The workaround is to put the `install(CODE)` in the cmake containing the target.
